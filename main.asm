;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Driver

;;; OpenBSD ELF header
	%ifdef OpenBSD
	section .note.openbsd.ident note
        align   2
        dd      8,4,1
        db      "OpenBSD",0
        dd      0
        align   2
	%endif
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	%include	"remember.def"
	%include 	"util.def"
	
	global _start

	%define	SYSCALL_MMAP	49
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section .text

_start:

	call 	init_remember

	mov	rdi, memory_buffer
	mov	rsi, 0xff
	call 	create_chunk_entry
	
	mov	rdi, 0
	jmp	exit

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section 	.data

	chunk_list_size	equ 4096
	
	chunks		dq 0x00
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	

	
