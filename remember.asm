;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; remember memory manager

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	%include "remember.def"
	%include "util.def"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section .text

init_remember:
	mov	rcx, max_chunk_count
	mov	rdi, chunks
init_remember_0:
	mov	[rdi], byte 0
	add	rdi, memory_chunk_size
	dec	rcx
	jnz	init_remember_0

	ret

create_chunk_entry:
	;; Start address in rdi
	;; Length in rsi

	;; Find the first chunk entry flagged with entry status of 0x00
	push	rdi
	push	rsi
	call	find_free_chunk	

	cmp	rdi, 0
	jnz	create_chunk_entry_0
	;; We didn't find a free chunk, so exit?

	mov	rdi, -1
	jmp	exit

create_chunk_entry_0:
	mov	byte [rdi + memory_chunk.entry_status], 0x01
	mov	byte [rdi + memory_chunk.mem_status], 0x01
	mov	[rdi + memory_chunk.start_address], rax
	mov	word [rdi + memory_chunk.length], si
	pop	rsi
	pop	rax

	ret

find_free_chunk:
	;; Cycle through all chunks to find first flagged 0x00
	mov	rbx, [chunk_count]
	mov	rdi, chunks
find_free_chunk_0:
	cmp	rbx, 0
	dec	rbx
	jz	find_free_chunk_1 ; We've reached the end of the chunk list
	mov	al, byte [rdi + memory_chunk.entry_status]
	cmp	al, 0x00
	jnz	find_free_chunk_0
	;; Return here as rdi will point to the free chunk
find_free_chunk_1:
	mov	rdi, 0
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	section .data

empty_chunk:
	istruc memory_chunk
	at memory_chunk.entry_status,	db 0
	at memory_chunk.mem_status,	db 0
	at memory_chunk.start_address, 	dq 0
	at memory_chunk.length,	 	dw 0
	iend

chunk_count:	dq	0
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	section .bss

	max_chunk_count   		equ	  1024
	initial_memory_size 	equ	  1024 * 1024 * 1024
	chunks			          resb  memory_chunk_size * max_chunk_count
	memory_buffer		      resb 	initial_memory_size

