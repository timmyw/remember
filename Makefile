all: remember

.phony: clean

ASM=nasm
ASMFLAGS=-D OpenBSD -f elf64 -g -F dwarf
LD=ld
LDFLAGS=-nopie -g

OBJS=remember.o main.o util.o

%.o : %.asm
	$(ASM) $(ASMFLAGS) -l $<.lst $< -o $@

remember: $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $(OBJS)

clean:
	rm -f *.o
	rm -f remember
