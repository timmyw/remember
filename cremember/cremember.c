#include <stdlib.h>
#include <sys/mman.h>
#include <stdio.h>
#include <string.h>

#define MAIN_MEM_SIZE  (16 * 1024 * 1024)
#define MAX_CHUNK_COUNT 100

#define SYSCALL_MMAP 0x09

void *main_memory = 0;
void *chunks = 0;

struct CHUNK {
  long address;
  long size;
  char status;
  char chunk_status;
};

void *alloc_memory(unsigned long long size);

int syscall_exit(int exit_code)
{
  int result;
  asm(
      "xor %%rax, %%rax\n"
      "movl $1, %%eax\n"
      "xor %%rdi, %%rdi\n"
      "movl %0, %%edi\n"
      "syscall\n"
      : "=r" (result)
      : "r" (exit_code)
      );
  printf("syscall error: %s\n", strerror(-1 * (long long)result));
  return result;
}

void* mmap_asm(unsigned long long size)
{
  void* result = 0;
  asm(
      "movq %1, %%rax\n" 	/* mmap */
      "movq $0, %%rdi\n"	/* address */
      "movq %2, %%rsi\n"	/* size */
      "movq %3, %%rdx\n"	/* PROT */
      "movq %4, %%rcx\n"	/* FLAGS */
      "movq $-1, %%r8\n"	/* fd */
      "movq $0, %%r9\n"		/* offset */
      "syscall\n"
      "mov %%rax, %0\n"
      : "=r" (result)
      : "n" (SYSCALL_MMAP),
	"g" (size),
	"n" (PROT_READ|PROT_WRITE),
	"n" (MAP_PRIVATE|MAP_ANONYMOUS)
      : "memory"
      );

  printf("result: %llx\n", (unsigned long long)result);
  return result;
}

/* void* mmap_asm_(unsigned long size) */
/* { */
/*   void* result = 0; */
/*   asm( */
/*       "mov $0, %%rdi\n\t"	/\* address *\/ */
/*       "mov %1, %%rax\n\t" 	/\* mmap *\/ */
/*       "mov %2, %%rsi\n\t"	/\* size *\/ */
/*       "mov %3, %%rdx\n\t"	/\* PROT *\/ */
/*       "mov %4, %%rcx\n\t"	/\* FLAGS *\/ */
/*       "mov $-1, %%r8\n\t"	/\* fd *\/ */
/*       "mov $0, %%r9\n\t"	/\* offset *\/ */
/*       "syscall\n\t" */
/*       "mov %%rax, %0\n\t"	/\* save the output *\/ */
/*       : "=r" (chunks) */
/*       : "r" (SYSCALL_MMAP), */
/* 	"r" (MAX_CHUNK_COUNT*sizeof(struct CHUNK)), */
/* 	"r" (PROT_READ|PROT_WRITE), */
/* 	"r" (MAP_PRIVATE|MAP_ANONYMOUS), */
/* 	"r" (0) // address */
/*       ); */
/*   printf("result: %llx\n", (unsigned long long)result); */
/*   return result; */
/* } */

void init_remember()
{
  /* main_memory = mmap(0, MAIN_MEM_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0); */
  /* /\* main_memory = mmap_asm(1024); *\/ */
  /* if ((long long)main_memory < 0) { */
  /*   printf("main error: %s\n", strerror(-1 * (long long)main_memory)); */
  /* } else { */
  /*   printf("main:   %llx\n", (unsigned long long)main_memory); */
  /* } */


  chunks = alloc_memory(MAX_CHUNK_COUNT*sizeof(struct CHUNK));
  /* chunks = mmap(0, MAX_CHUNK_COUNT*sizeof(struct CHUNK), PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0); */

  if ((long long)chunks < 0) {
    printf("chunks error: %s\n", strerror(-1 * (long long)chunks)); 
  } else {
    printf("chunks:   %llx\n", (unsigned long long)chunks);
  }
  *(char *)chunks = 0x7f;
  *((char *)chunks+4095) = 0x7f;
  
}

void shutdown_remember()
{
  munmap(main_memory, MAIN_MEM_SIZE);
}

int main(int argc, char* argv[])
{
  init_remember();

  /* *(char *)main_memory = 65; */
  /* while (1) {;} */
  
  shutdown_remember();
  return 0;
}
