	.globl alloc_memory

	/* Size is in %rdi */
alloc_memory:
	movq $0x09, %rax
	movq %rdi, %rsi
	movq $0, %rdi
	movq $0x03, %rdx
	movq $0x22, %rcx
	movq $-1, %r8 		/* fd */
	movq $0, %r9		/* offset*/
	syscall
	ret

	/*
	0x20 | 0x02
	*/
